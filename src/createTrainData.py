'''
Binary training dataset 
Usage: python3 createTrainData.py
EX: 
python3 createTrainData.py
it's vs its 
you're vs your 
they're vs their
loose vs lose 
to vs too
'''
import sys
import getopt
import nltk
class_it_s     = "it_s"
class_its      = "its"
class_you_re   = "you_re"
class_your     = "your"
class_they_re  = "they_re"
class_their    = "their"
class_loose    = "loose"
class_lose     = "lose"
class_to       = "to"
class_too      = "too"
dataset_filename = "../one_meelyun_sentences"
train_filename_its   = "its_train_data.txt"
train_filename_your  = "your_train_data.txt"
train_filename_their = "their_train_data.txt"
train_filename_lose  = "lose_train_data.txt"
train_filename_too   = "too_train_data.txt"
isIncludeSelf = True #default: Include the class word
isDoubleFeatureLine = False #default: double the same features, one with class word and one not

def usage():
	print("Usage: puthon3 createTrainData.py [-l][-n][-d]".format(sys.argv[0]))

def checkDevFileAndCommandArgu():
	global class_it_s, class_its, class_you_re, class_your,class_they_re, class_their, class_loose, class_lose, class_to, class_too, dataset_filename,train_filename_its, train_filename_your, train_filename_their, train_filename_lose,train_filename_too, isIncludeSelf, isDoubleFeatureLine
	try:
		optlist, args = getopt.gnu_getopt(sys.argv[1:], 'lnd')
		for opt,arg in optlist:
			if opt in ("-l"):
				isIncludeSelf = True
			elif opt in ("-n"):
				isIncludeSelf = False
			elif opt in ("-d"):
				isDoubleFeatureLine = True
	except getopt.GetoptError:
		print("GetoptError")
		usage()
		sys.exit(1)

def createFeatureForSingleLine_includeSelf(line, outfile_its, outfile_your,outfile_their, outfile_loose ,outfile_too):
	global class_it_s, class_its, class_you_re, class_your,class_they_re, class_their, class_loose, class_lose, class_to, class_too, dataset_filename,train_filename_its, train_filename_your, train_filename_their, train_filename_lose,train_filename_too, isIncludeSelf, isDoubleFeatureLine
	LL  = "LLword "
	L   = "Lword "
	R   = " Rword"
	RR  = " RRword"
	line = LL+L+line+R+RR
	tokens = nltk.word_tokenize(line)
	#print(tokens)
	tagged = nltk.pos_tag(tokens)
	for i in range(2,len(tokens)-2):
		if tokens[i] == class_it_s or tokens[i] == class_its:
			if tokens[i] == class_it_s:
				print(class_it_s+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_its)
			elif tokens[i] == class_its:
				print(class_its+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_its)
		elif tokens[i] == class_you_re or tokens[i] == class_your:
			if tokens[i] == class_you_re:
				print(class_you_re+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_your)
			elif tokens[i] == class_your:
				print(class_your+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_your)
		elif tokens[i] == class_they_re or tokens[i] == class_their:
			if tokens[i] == class_they_re:
				print(class_they_re+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_their)
			elif tokens[i] == class_their:
				print(class_their+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_their)
		if tokens[i] == class_loose or tokens[i] == class_lose:
			if tokens[i] == class_loose:
				print(class_loose+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_loose)
			elif tokens[i] == class_lose:
				print(class_lose+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_loose)
		if tokens[i] == class_to or tokens[i] == class_too:
			if tokens[i] == class_to:
				print(class_to+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_too)
			elif tokens[i] == class_too:
				print(class_too+" LL="+tokens[i-2]+" L="+tokens[i-1]+" R="+tokens[i+1]+" RR="+tokens[i+2]+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1], file=outfile_too)

def createTrainFile():
	global class_it_s, class_its, class_you_re, class_your,class_they_re, class_their, class_loose, class_lose, class_to, class_too, dataset_filename,train_filename_its, train_filename_your, train_filename_their, train_filename_lose,train_filename_too, isIncludeSelf, isDoubleFeatureLine
	infile = open(dataset_filename,'r')
	outfile_its = open(train_filename_its, 'w')
	outfile_your  = open(train_filename_your, 'w')
	outfile_their = open(train_filename_their, 'w')
	outfile_loose = open(train_filename_lose, 'w')
	outfile_too   = open(train_filename_too, 'w')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		line = line.lower()
		line = line.replace("it's", "it_s")
		line = line.replace("they're", "they_re")
		line = line.replace("you're", "you_re")
		if class_it_s not in line and class_its not in line and class_you_re not in line and class_your not in line and class_they_re not in line and class_their not in line and class_loose not in line and class_lose not in line and class_to not in line and class_too not in line:
			continue
		else:
			if isIncludeSelf==True and isDoubleFeatureLine==False:
				createFeatureForSingleLine_includeSelf(line, outfile_its, outfile_your,outfile_their, outfile_loose ,outfile_too)
	outfile_its.close()
	outfile_your.close()
	outfile_their.close()
	outfile_loose.close()
	outfile_too.close()


def main():
	global class_it_s, class_its, class_you_re, class_your,class_they_re, class_their, class_loose, class_lose, class_to, class_too, dataset_filename,train_filename_its, train_filename_your, train_filename_their, train_filename_lose,train_filename_too, isIncludeSelf, isDoubleFeatureLine
	checkDevFileAndCommandArgu()
	'''print(class_1_name)
	print(class_2_name)
	print(dataset_filename)
	print(train_filename)
	print("isIncludeSelf="+str(isIncludeSelf))
	print("isDoubleFeatureLine="+str(isDoubleFeatureLine))'''
	createTrainFile()



if __name__ == '__main__':
	main()