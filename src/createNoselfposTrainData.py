'''
python3 createNoselfposTrainData.py
'''

train_filename_its   = "hw3_testdata/its_train_data.txt"
train_filename_your  = "hw3_testdata/your_train_data.txt"
train_filename_their = "hw3_testdata/their_train_data.txt"
train_filename_lose  = "hw3_testdata/lose_train_data.txt"
train_filename_too   = "hw3_testdata/too_train_data.txt"
noselfpos_train_filename_its   = "hw3_testdata/its_train_data_noselfpos.txt"
noselfpos_train_filename_your  = "hw3_testdata/your_train_data_noselfpos.txt"
noselfpos_train_filename_their = "hw3_testdata/their_train_data_noselfpos.txt"
noselfpos_train_filename_lose  = "hw3_testdata/lose_train_data_noselfpos.txt"
noselfpos_train_filename_too   = "hw3_testdata/too_train_data_noselfpos.txt"
its_list   = []
your_list  = []
their_list = []
lose_list  = []
too_list   = []

def copy():
	infile1 = open(train_filename_its,'r')
	lines = infile1.readlines()
	infile1.close()
	for line in lines:
		line = line.rstrip('\n')
		words = line.split()
		words.pop(7)
		its_list.append(" ".join(words))
	
	infile2 = open(train_filename_your,'r')
	lines = infile2.readlines()
	infile2.close()
	for line in lines:
		line = line.rstrip('\n')
		words = line.split()
		words.pop(7)
		your_list.append(" ".join(words))

	infile3 = open(train_filename_their,'r')
	lines = infile3.readlines()
	infile3.close()
	for line in lines:
		line = line.rstrip('\n')
		words = line.split()
		words.pop(7)
		their_list.append(" ".join(words))

	infile4 = open(train_filename_lose,'r')
	lines = infile4.readlines()
	infile4.close()
	for line in lines:
		line = line.rstrip('\n')
		words = line.split()
		words.pop(7)
		lose_list.append(" ".join(words))

	infile5 = open(train_filename_too,'r')
	lines = infile5.readlines()
	infile5.close()
	for line in lines:
		line = line.rstrip('\n')
		words = line.split()
		words.pop(7)
		too_list.append(" ".join(words))


def create():
	outfile1 = open(noselfpos_train_filename_its, 'w')
	for line in its_list:
		print(line, file=outfile1)
	outfile2 = open(noselfpos_train_filename_your, 'w')
	for line in your_list:
		print(line, file=outfile2)
	outfile3 = open(noselfpos_train_filename_their, 'w')
	for line in their_list:
		print(line, file=outfile3)
	outfile4 = open(noselfpos_train_filename_lose, 'w')
	for line in lose_list:
		print(line, file=outfile4)
	outfile5 = open(noselfpos_train_filename_too, 'w')
	for line in too_list:
		print(line, file=outfile5)

def main():
	copy()
	create()
	

if __name__ == '__main__':
	main()