#******************************Call by module*******************************************
def initializeByReadingModelFile(modelFileName):
	pClasses = {} #{class1: probability_class1, class2: probability_class2}
	pWordByClasses = {} ##{class1:wordProbDict1, class2:wordProbDict2...}
	unknownWordProbByClassDict = {} #{{class1:class1_unknownWordProb, class2:class2_unknownWordProb....}}
	pClasses={}
	infile = open(modelFileName,'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		words = line.split()
		if words[0]=='pClass':
			pClasses[words[1]] = words[2]
		elif words[0]=='pUnknown':
			unknownWordProbByClassDict[words[1]] = words[2]
		else:
			className = words[0]
			word = words[1]
			probabilityLog = words[2]
			pWordByClassDict = None
			if className in pWordByClasses:
				pWordByClassDict = pWordByClasses[className]
			else:
				pWordByClassDict = {}
				pWordByClasses[className] = pWordByClassDict
			pWordByClassDict[word] = probabilityLog #p(word|class) (log10)
	return pClasses, unknownWordProbByClassDict, pWordByClasses

def doClassify(pClasses, unknownWordProbByClassDict, pWordByClasses, line):
	finalProbByClass = {} #{ P(class1|w), P(class2|w), P(class3|w),.....}
	words = line.split()
	for className in pClasses:
		nominator = float(pClasses[className])
		for word in words:
			if word in pWordByClasses[className]:
				nominator += float(pWordByClasses[className][word])
			else:
				nominator += float(unknownWordProbByClassDict[className])
		finalProbByClass[className] = nominator
	maxProb = max(finalProbByClass.values())
	finalClassList = [x for x,y in finalProbByClass.items() if y == maxProb]
	return finalClassList[0]

#******************************Call by module*******************************************


def main():
	print("Error: should be called as a module!")

if __name__ == '__main__':
	main()
#else:
	#print("nblearn loaded as a module")