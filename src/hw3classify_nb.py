'''
Usage: python3 hw3classify_nb.py modelFile1 modelFile2 modelFile3 modelFile4 modelFile5
EX: python3 hw3classify_nb.py its_model lose_model their_model your_model too_model
EX: python3 hw3classify_nb.py its_model lose_model their_model your_model too_model <hw3_testdata/hw3.dev.err.txt >hw3_testdata/output_noself.txt
EX: python3 hw3classify_nb.py its_model_selfWord lose_model_selfWord their_model_selfWord your_model_selfWord too_model_selfWord <hw3_testdata/hw3.dev.err.txt >hw3_testdata/output_selfWord.txt
EX: python3 hw3classify_nb.py its_model_noselfpos lose_model_noselfpos their_model_noselfpos your_model_noselfpos too_model_noselfpos <hw3_testdata/hw3.dev.err.txt >hw3_testdata/output_noselfpos.txt
'''
import nltk
import sys
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import nbclassify
its_model   = sys.argv[1]
lose_model  = sys.argv[2]
their_model = sys.argv[3]
your_model  = sys.argv[4]
too_model   = sys.argv[5]
class_it_s     = "it_s"
class_its      = "its"
class_you_re   = "you_re"
class_your     = "your"
class_they_re  = "they_re"
class_their    = "their"
class_loose    = "loose"
class_lose     = "lose"
class_to       = "to"
class_too      = "too"
pClasses_its={}
unknownWordProbByClassDict_its={}
pWordByClasses_its={}
pClasses_lose={}
unknownWordProbByClassDict_lose={}
pWordByClasses_lose={}
pClasses_their={}
unknownWordProbByClassDict_their={}
pWordByClasses_their={}
pClasses_your={}
unknownWordProbByClassDict_your={}
pWordByClasses_your={}
pClasses_too={}
unknownWordProbByClassDict_too={}
pWordByClasses_too={}

def initialize():
	#print("Initialize....")
	global pClasses_its,unknownWordProbByClassDict_its,pWordByClasses_its,pClasses_lose,unknownWordProbByClassDict_lose,pWordByClasses_lose
	global pClasses_their,unknownWordProbByClassDict_their,pWordByClasses_their,pClasses_your, unknownWordProbByClassDict_your, pWordByClasses_your
	global pClasses_too, unknownWordProbByClassDict_too, pWordByClasses_too
	pClasses_its,unknownWordProbByClassDict_its,pWordByClasses_its = nbclassify.initializeByReadingModelFile(its_model)
	pClasses_lose,unknownWordProbByClassDict_lose,pWordByClasses_lose = nbclassify.initializeByReadingModelFile(lose_model)
	pClasses_their,unknownWordProbByClassDict_their,pWordByClasses_their = nbclassify.initializeByReadingModelFile(their_model)
	pClasses_your, unknownWordProbByClassDict_your, pWordByClasses_your = nbclassify.initializeByReadingModelFile(your_model)
	pClasses_too, unknownWordProbByClassDict_too, pWordByClasses_too = nbclassify.initializeByReadingModelFile(too_model)
	#Testing:
	'''for w in pClasses_its:
		print("Debug1 "+w)
	for w in pClasses_lose:
		print("Debug2 "+w)
	for w in pClasses_their:
		print("Debug3 "+w)
	for w in pClasses_your:
		print("Debug4 "+w)
	for w in pClasses_too:
		print("Debug5 "+w)'''

def correct():#improved efficiency! => call nbclassify as a module!
	global pClasses_its,unknownWordProbByClassDict_its,pWordByClasses_its,pClasses_lose,unknownWordProbByClassDict_lose,pWordByClasses_lose
	global pClasses_their,unknownWordProbByClassDict_their,pWordByClasses_their,pClasses_your, unknownWordProbByClassDict_your, pWordByClasses_your
	global pClasses_too, unknownWordProbByClassDict_too, pWordByClasses_too
	while True:
		try:
			sentence = input()
			if sentence=="":
				print("", end="", flush=True)
			elif sentence==None:
				break
			LL  = "LLword "
			L   = "Lword "
			R   = " Rword"
			RR  = " RRword"
			line = LL+L+sentence+R+RR
			line = line.replace("it's", "it_s")
			line = line.replace("they're", "they_re")
			line = line.replace("you're", "you_re")
			words = line.split()
			tagged = nltk.pos_tag(words)
			ret=""
			for i in range(2,len(words)-2):
				testWord = words[i].lower()
				if testWord!=class_it_s and testWord!=class_its and testWord!=class_you_re and testWord!=class_your and testWord!=class_they_re and testWord!=class_their and testWord!=class_loose and testWord!=class_lose and testWord!=class_to and testWord!=class_too:
					ret = ret + words[i] +" "
					continue
				else:
					#print("Debug_testWord = "+testWord+", realWord = "+words[i])
					#errorword   = "LL="+words[i-2].lower()+" L="+words[i-1].lower()+" curWord="+words[i].lower()+" R="+words[i+1].lower()+" RR="+words[i+2].lower()+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1]
					#errorword   = "LL="+words[i-2].lower()+" L="+words[i-1].lower()+" R="+words[i+1].lower()+" RR="+words[i+2].lower()+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" curPOS="+tagged[i][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1]
					errorword   = "LL="+words[i-2].lower()+" L="+words[i-1].lower()+" R="+words[i+1].lower()+" RR="+words[i+2].lower()+" LLPOS="+tagged[i-2][1]+" LPOS="+tagged[i-1][1]+" RPOS="+tagged[i+1][1]+" RRPOS="+tagged[i+2][1]
					correctWord = ""
					#print("Debug_errorWord = "+errorword)
					if testWord==class_it_s or testWord==class_its:
						#print("Debug_its")
						correctWord = str(nbclassify.doClassify(pClasses_its, unknownWordProbByClassDict_its, pWordByClasses_its, errorword))
					elif testWord==class_loose or testWord==class_lose:
						#print("Debug_loose")
						correctWord = str(nbclassify.doClassify(pClasses_lose, unknownWordProbByClassDict_lose, pWordByClasses_lose, errorword))
					elif testWord==class_they_re or testWord==class_their:
						#print("Debug_they_re")
						correctWord = str(nbclassify.doClassify(pClasses_their, unknownWordProbByClassDict_their, pWordByClasses_their, errorword))
					elif testWord==class_you_re or testWord==class_your:
						#print("Debug_you_re")
						correctWord = str(nbclassify.doClassify(pClasses_your, unknownWordProbByClassDict_your, pWordByClasses_your, errorword))
					elif testWord==class_to or testWord==class_too:
						#print("Debug_to")
						correctWord = str(nbclassify.doClassify(pClasses_too, unknownWordProbByClassDict_too, pWordByClasses_too, errorword))
					#print("Debug_correctWord= "+correctWord)
					if correctWord=="it_s":
						correctWord="it's"
					elif correctWord=="they_re":
						correctWord="they're"
					elif correctWord=="you_re":
						correctWord="you're"
					correctWord = list(correctWord)
					correctWord[0] = words[i][0]
					correctWord = "".join(correctWord)
					ret = ret + correctWord + " "
			print(ret)
		except EOFError:
			break

def main():
	initialize()
	correct()

if __name__ == '__main__':
	main()