**Error detection and correction: dealing with homophone confusion**
===============================================================



    

> This is Homework 3 at USC CSCI 544 Natural Language Processing. In written language, errors involving the use of words that sound similar or the same are fairly common. For example, the word its is frequently used where it's should, and vice-versa. In this assignment, I developed an approach for detecting and correcting such errors.

Here, I only deal with these specific types of confusion:

 - it's vs its
 - you're vs your
 - they're vs their
 - loose vs lose
 - to vs too

----------

**Abstract**
-------------------------------------

>Because there are five cases I need to deal with, I first collected data from Wikipedia and transformed it to five training files for five cases. Then, I use Naive Bayes Theory to learn and create five model files. Lastly, I created a Naive Bayes Classifier which includes five binary classifiers to deal with five cases, individually. This classifier read test data and output the correct data with five model files.

**Implementation**
-------------------------------------
Step 1: Collect Training Data
-------------------------------------

>Training text data can be dumped from Wikipedia website from here: http://en.wikipedia.org/wiki/Wikipedia:Database_download
> I dumped, transformed, and arranged it to what I want, namely plain text data and one sentence per line.  

Step 2: Create Training file
-------------------------------------

>Because here are five cases I need to deal with, I transformed and arranged raw data with the format I need Then, I wrote a script (createTrainData.py) to generate five training files for five cases, individually. The following is the usage of the script to generate the training file.

**Usage:**
```
$ python3 createTrainData.py

```
It generates the following five training files:

 - its_train_data.txt
 - your_train_data.txt
 - their_train_data.txt
 - lose_train_data.txt
 - too_train_data.txt
 
Step 3: Learn from data
-------------------------------------

>Here, I use Naive Bayes Theory (nblearn.py) to learn and train from training data and generating five model for five cases, individually.

**Usage:**
```
$ python3 nblearn.py its_train_data.txt its_model
```

It generates the following five model files:

 - its_model 
 - lose_model 
 - their_model 
 - your_model 
 - too_model

Step 4: Classification
-------------------------------------

>Finally, I wrote a script (hw3classify_nb.py) to use Naive Bayes Classifier (nbclassify.py) I created before to classify these five cases. This script stdin sentences, do classification, and output to stdout.

**Usage:**
```
$ python3 hw3classify_nb.py its_model lose_model their_model your_model too_model <hw3.dev.err.txt >output.txt
```

It generates the result to stdout or redirect to a file.


----------


**Result**
-------------------------------------



>I use nearby words and nearby Part-Of-Speech tags to be my training and classification features, including pre-pre-word, pre-word, next-word, next-next-word, pre-pre-POS, pre-POS, next-POS, and next-next-POS. Also, I found that excluding current word and POS of current word can get the overall best accuracy result. It is because current word and POS of current word will dominate the prediction when they are included as features. I provide numeric results each cases in the following as a reference. The test data are hw3.dev.err.txt and hw3.dev.txt.

**Feature set 1:**
Exclude current word and POS of current word:
```
Class LL:word1 L:word2 R:word3 RR:word4 LLP:POS1 LP:POS2 RP:POS3 RRP:POS4
```

Class      | Total num | Error num | ErrorRate
---------- | --------- | --------- | ----------
its/it's   | 1372 | 59 | 0.043002915451895045
you're or your | 809 | 97 | 0.11990111248454882
they're or their | 2026 | 25 | 0.012339585389930898
loose or lose | 70 | 8 | 0.11428571428571428
too or to | 20354 | 428 | 0.02102780780190626

**Feature set 2:**
Exclude current word but include POS of current word:
```
Class LL:word1 L:word2 R:word3 RR:word4 LLP:POS1 LP:POS2 cur:POS3 RP:POS4 RRP:POS5
```

Class      | Total num | Error num | ErrorRate
---------- | --------- | --------- | ----------
its/it's   | 1372 | 209 | 0.152332361516035
you're or your | 809 | 190 | 0.23485784919653893
they're or their | 2026 | 54 | 0.02665350444225074
loose or lose | 70 | 6 | 0.08571428571428572
too or to | 20354 | 1547 | 0.07600471651763781

**Feature set 3:**
Include current word and POS of current word:
```
Class LL:word1 L:word2 cur:word3 R:word4 RR:word5 LLP:POS1 LP:POS2 cur:POS3 RP:POS4 RRP:POS5
```

Class      | Total num | Error num | ErrorRate
---------- | --------- | --------- | ----------
its/it's   | 1372 | 405 | 0.29518950437317787
you're or your | 809 | 253 | 0.3127317676143387
they're or their | 2026 | 204 | 0.10069101678183613
loose or lose | 70 | 9 | 0.12857142857142856
too or to | 20354 | 4690 | 0.23042153876387933